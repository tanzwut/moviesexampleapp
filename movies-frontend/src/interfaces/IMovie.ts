import { IActor } from "./IActor"
import { IGenre } from "./IGenre"

export interface IMovie {
  id: Number
  title: string
  genre: IGenre
  imageUri: string
  actors: [IActor]
}