export interface IGenre {
  id: Number
  name: string
}