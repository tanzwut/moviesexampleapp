import { IMovie } from "./IMovie"

export interface IActor {
  id: Number
  name: string
  movies: [IMovie]
}