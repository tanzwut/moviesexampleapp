"use client"

import { IMovie } from "@/interfaces/IMovie"
import Image from "next/image"

type Props = {
  movie: IMovie
}

function MovieCard({ movie }: Props) {
  const truncatedTitle = movie.title.length > 25 ? movie.title.substring(0, 25) + '...' : movie.title

  return (
    <div className="w-64 h-96 relative">
      <Image src={movie.imageUri} alt={movie.title} fill className="rounded-md" style={{
        objectFit: "cover"
      }} />
      <div class="absolute w-full py-2.5 bottom-0 inset-x-0 bg-neutral-700 text-white text-xs text-center leading-4 rounded-b-md">
        <h3>{truncatedTitle}</h3>
      </div>
    </div>
  )
}

export default MovieCard