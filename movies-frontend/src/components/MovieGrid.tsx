import { IMovie } from "@/interfaces/IMovie";
import MovieCard from "./MovieCard";

type Props = {
  movies: IMovie[]
}

function MovieGrid({ movies }: Props) {
  return (
    <div className=" w-auto h-auto flex justify-center">
      <div className="mt-6 grid grid-cols-1 md:grid-cols-3 gap-5">
        {
          movies.map(movie => {
            return <MovieCard key={`${movie.id}`} movie={movie} />
          })
        }
      </div>
    </div>
  )
}

export default MovieGrid