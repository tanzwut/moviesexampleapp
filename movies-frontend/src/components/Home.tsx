"use client"

import { useEffect, useState } from "react"
import MovieGrid from "./MovieGrid"
import { IMovie } from "@/interfaces/IMovie"
import { BASE_URL } from "@/app/constants";
import { useSearchParams } from "next/navigation";

async function getMovies(): Promise<any> {
  try {
    const response = await fetch(`${BASE_URL}/movieS`);
    return await response.json();
  }
  catch (error) {
    console.log(error);
    return []
  }
}

async function searchMovie(query: string): Promise<any> {
  try {
    const response = await fetch(`${BASE_URL}/Movies/search?value=${query}`)

    return await response.json();
  }
  catch (error) {
    console.log(error);
    return []
  }
}

function Home() {
  const [movies, setMovies] = useState<IMovie[]>([]);

  const searchParams = useSearchParams();
  const searchQuery = searchParams && searchParams.get("q");

  async function initializeMovies() {
    const results = await getMovies();

    console.log(results);


    setMovies(results)
  }

  useEffect(() => {
    initializeMovies();
  }, []);

  useEffect(() => {
    const handleSearch = async () => {
      if (searchQuery) {
        const results = await searchMovie(searchQuery);
        setMovies(results);
      }
      else {
        initializeMovies();
        return true;
      }
    };

    handleSearch();
  }, [searchQuery]);

  return (
    <MovieGrid movies={movies} />
  )
}

export default Home