import Home from "@/components/Home";
import MovieGrid from "@/components/MovieGrid";
import { IMovie } from "@/interfaces/IMovie";

export default function HomePage() {
  return (
    <Home />
  );
}
