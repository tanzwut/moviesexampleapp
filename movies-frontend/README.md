# Frontend

This is the frontend project for **Movies Example App**.
Made with React, Next.js and Tailwind CSS.
 ## Requirements
 

 - [Node.Js 21.6.1+](https://nodejs.org/)

## Installation
Use a package manager to install dependencies from **package.json**
```bash
npm install

# or

yarn  install

# or

pnpm  install

# or

bun  install
```
## Getting Started

  

First, run the development server:

  

```bash

npm  run  dev

# or

yarn  dev

# or

pnpm  dev

# or

bun  dev

```

  

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

  

## Learn More

  

To learn more about Next.js, take a look at the following resources:

  

- [Next.js Documentation](https://nextjs.org/docs) - learn about Next.js features and API.

- [Learn Next.js](https://nextjs.org/learn) - an interactive Next.js tutorial.