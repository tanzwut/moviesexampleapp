﻿using System.Text.Json.Serialization;

namespace MoviesBackend.Entities
{
    public class Actor
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public ICollection<Movie> Movies { get; set; } = [];
    }
}
