﻿using System.ComponentModel.DataAnnotations;

namespace MoviesBackend.Entities
{
    public class Movie
    {

        public int Id { get; set; }
        public string Title { get; set; }
        public int GenreId { get; set; }
        public Genre Genre { get; set; }
        [Url]
        public string ImageUri { get; set; }
        public ICollection<Actor> Actors { get; set; } = [];
    }
}
