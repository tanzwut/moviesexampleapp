﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MoviesBackend.Contexts;
using MoviesBackend.DTOs;
using MoviesBackend.Entities;

namespace MoviesBackend.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class MoviesController : ControllerBase
    {
        private readonly MoviesDbContext _context;
        private readonly IMapper _mapper;

        public MoviesController(MoviesDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Movies
        [HttpGet]
        public async Task<ActionResult<IEnumerable<MovieDTO>>> GetMovies()
        {
            var movies = await _context.Movies.Include(m => m.Genre).Include(m => m.Actors).ToListAsync();

            var moviesDTO = _mapper.Map<List<MovieDTO>>(movies);

            return moviesDTO; 
        }

        // GET: api/Movies/5
        [HttpGet("{id}")]
        public async Task<ActionResult<MovieDTO>> GetMovie(int id)
        {
            var movie = await _context.Movies.Include(m => m.Genre).Include(m => m.Actors).FirstOrDefaultAsync(m => m.Id == id);

            if (movie == null)
            {
                return NotFound();
            }

            return _mapper.Map<MovieDTO>(movie);
        }

        // GET: api/Movies/search
        [HttpGet("search")]
        public async Task<ActionResult<IEnumerable<MovieDTO>>> GetMoviesSearch(string value)
        {
            var movies = _context.Movies.Include(m => m.Genre).Include(m => m.Actors).AsQueryable();

            var movieTitles = await movies.Where(m => m.Title.ToLower().Contains(value.ToLower())).ToListAsync();
            var movieGenre = await movies.Where(m => m.Genre.Name.ToLower().Contains(value.ToLower())).ToListAsync();
            var movieActors = await movies.Where(m => m.Actors.Any(a => a.Name.ToLower().Contains(value.ToLower()))).ToListAsync();

            var result = movieTitles.Union(movieGenre).Union(movieActors);

            var moviesDTO = _mapper.Map<List<MovieDTO>>(result);

            return moviesDTO;
        }
    }
}
