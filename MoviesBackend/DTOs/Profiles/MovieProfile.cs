﻿using AutoMapper;
using MoviesBackend.Entities;

namespace MoviesBackend.DTOs.Profiles
{
    public class MovieProfile : Profile
    {
        public MovieProfile()
        {
            CreateMap<Movie, MovieDTO>();
            CreateMap<Movie, MovieActorDTO>();
        }
    }
}
