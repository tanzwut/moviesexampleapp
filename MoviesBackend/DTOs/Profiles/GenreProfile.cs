﻿using AutoMapper;
using MoviesBackend.Entities;

namespace MoviesBackend.DTOs.Profiles
{
    public class GenreProfile : Profile
    {
        public GenreProfile()
        {
            CreateMap<Genre, GenreDTO>();
        }
    }
}
