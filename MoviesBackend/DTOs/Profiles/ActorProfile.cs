﻿using AutoMapper;
using MoviesBackend.Entities;

namespace MoviesBackend.DTOs.Profiles
{
    public class ActorProfile : Profile
    {
        public ActorProfile()
        {
            CreateMap<Actor, ActorDTO>();
            CreateMap<Actor, ActorMovieDTO>();
        }
    }
}
