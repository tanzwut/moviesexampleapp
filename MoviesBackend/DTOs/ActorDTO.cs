﻿namespace MoviesBackend.DTOs
{
    public record ActorDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public ICollection<MovieActorDTO> Movies { get; set; }
    }
}
