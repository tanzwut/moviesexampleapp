﻿using System.ComponentModel.DataAnnotations;

namespace MoviesBackend.DTOs
{
    public record MovieActorDTO
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public GenreDTO Genre { get; set; }
        [Url]
        public string ImageUri { get; set; }
    }
}
