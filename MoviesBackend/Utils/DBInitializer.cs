﻿using MoviesBackend.Contexts;
using MoviesBackend.Entities;

namespace MoviesBackend.Utils
{
    public static class DBInitializer
    {
        public static WebApplication Seed(this WebApplication app)
        {
            using (var scope = app.Services.CreateScope())
            {
                using var context = scope.ServiceProvider.GetRequiredService<MoviesDbContext>();

                try
                {
                    context.Database.EnsureCreated();

                    var movies = context.Movies.FirstOrDefault();

                    if (movies == null)
                    {
                        var genres = new List<Genre>() { 
                            new Genre { Name = "Action" },
                            new Genre { Name = "Comedy" },
                            new Genre { Name = "Romance" },
                            new Genre { Name = "Thriller" } 
                        };

                        context.Genres.AddRange(genres);

                        context.Movies.AddRange(
                            new Movie { Title = "Die Hard", GenreId = 1, ImageUri = "https://moviepostermexico.com/cdn/shop/products/2_2b9c820b-90ba-469d-9361-6bb819570634_1024x1024@2x.jpg?v=1596172699" },
                            new Movie { Title = "The Naked Gun", GenreId = 2, ImageUri = "https://cdnx.jumpseller.com/peliculasdepapel/image/33020464/the_naked_gun_1.jpg?1678460971" },
                            new Movie { Title = "How to Lose a Guy in 10 Days", GenreId = 3, ImageUri = "https://m.media-amazon.com/images/M/MV5BMjE4NTA1NzExN15BMl5BanBnXkFtZTYwNjc3MjM3._V1_.jpg" },
                            new Movie { Title = "Oppenheimer", GenreId = 4, ImageUri = "https://moviepostermexico.com/cdn/shop/files/oppenheimer_ver3_xxlg_1024x1024@2x.jpg?v=1690337282" }
                            );

                        context.SaveChanges();

                        var dbMovies = context.Movies.ToList();

                        var actors = new List<Actor>() {
                            new Actor { Name = "Bruce Willis", Movies = [dbMovies[0]] }, // 1
                            new Actor { Name = "Alan Rickman", Movies = [dbMovies[0]] }, // 2
                            new Actor { Name = "Leslie Nielsen", Movies = [dbMovies[1]] }, // 3
                            new Actor { Name = "Priscilla Presley", Movies = [dbMovies[1]] }, // 4
                            new Actor { Name = "George Kennedy", Movies = [dbMovies[1]] }, // 5
                            new Actor { Name = "Kate Hudson", Movies = [dbMovies[2]] }, // 6
                            new Actor { Name = "Matthew McConaughey", Movies = [dbMovies[2]] }, // 7
                            new Actor { Name = "Kathryn Han", Movies = [dbMovies[2]] }, // 8
                            new Actor { Name = "Cillian Murphy", Movies = [dbMovies[3]] } // 9
                        };
                        context.Actors.AddRange(actors);

                        context.SaveChanges();

                        var dbActors = context.Actors.ToList();

                        List<Actor> dieHardActors = [dbActors[0], dbActors[1]];
                        List<Actor> nakedGunActors = [dbActors[2], dbActors[3], dbActors[4]];
                        List<Actor> loseManActors = [dbActors[5], dbActors[6], dbActors[7]];
                        List<Actor> oppenheimerActors = [dbActors[8]];

                        context.Movies.Find(1)!.Actors = dieHardActors;
                        context.Movies.Find(2)!.Actors = nakedGunActors;
                        context.Movies.Find(3)!.Actors = loseManActors;
                        context.Movies.Find(4)!.Actors = oppenheimerActors;

                        context.SaveChanges();
                    }
                }
                catch (Exception)
                {
                    throw;
                }

                return app;
            }
        }
    }
}
