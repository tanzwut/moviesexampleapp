﻿using Microsoft.EntityFrameworkCore;
using MoviesBackend.Entities;

namespace MoviesBackend.Contexts
{
    public class MoviesDbContext: DbContext
    {
        public DbSet<Genre> Genres { get; set; }
        public DbSet<Actor> Actors { get; set; }
        public DbSet<Movie> Movies { get; set; }

        public MoviesDbContext(DbContextOptions<MoviesDbContext> options) : base(options) { }
    }
}
