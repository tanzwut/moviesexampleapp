# Backend

This is the frontend project for **Movies Example App**.
Made with C# and ASP.&#8204;NET Core Web API
 ## Requirements
 

 - .NET 8 SDK

## Setting Up

Open a terminal in the root folder and then run
```bash
dotnet restore
```
That will install the tools and dependencies for the project.

## Getting Started

First, run the development server:

```bash
dotnet run
```

Then, open [localhost:5022/swagger](http://localhost:5022/swagger) to see REST API documentation and endpoints.

## Learn More

  

To learn more about ASP.&#8204;NET Core Web API, take a look at the following resources:

  

- [ASP.&#8204;NET Core web API Documentation](https://learn.microsoft.com/en-us/aspnet/core/tutorials/web-api-help-pages-using-swagger?view=aspnetcore-8.0) - ASP.&#8204;NET Core web API documentation with Swagger / OpenAPI | Microsoft Learn