# Movies App
Movies app is a small movies explorer where you can search movies by title, genre and actor.

## Backend

Backend was made with ASP.&#8204;Net Core API and C#. You could access to backend folder if you click [here](/MoviesBackend)

## Frontend
Frontend was made with React and NextJs. You could access to frontend folder if you click [here](/movies-frontend).